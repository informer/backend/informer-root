package pl.wat.informer.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.wat.informer.dto.form.AdminRegisterForm;
import pl.wat.informer.exception.DuplicatedUserException;
import pl.wat.informer.model.Administrator;
import pl.wat.informer.model.User;
import pl.wat.informer.repository.AdministratorRepository;
import pl.wat.informer.repository.UserRepository;

@Service
public class AdministratorService {

    private final AdministratorRepository administratorRepository;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final RoleService roleService;

    public AdministratorService(AdministratorRepository administratorRepository, UserRepository userRepository,
                                BCryptPasswordEncoder bCryptPasswordEncoder, RoleService roleService) {
        this.administratorRepository = administratorRepository;
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.roleService = roleService;
    }

    public void add(AdminRegisterForm adminRegisterForm){
        if(isEmailDuplicated(adminRegisterForm.getEmail())) throw new DuplicatedUserException(adminRegisterForm.getEmail());
        Administrator admin = buildUserToRegister(adminRegisterForm);
        administratorRepository.save(admin);
    }

    private Administrator buildUserToRegister(AdminRegisterForm in){
        Administrator admin = Administrator.builder()
                .firstName(in.getFirstName())
                .lastName(in.getLastName())
                .user(User.builder()
                        .username(in.getEmail())
                        .password(bCryptPasswordEncoder.encode(in.getPassword()))
                        .role(roleService.findRoleAdministrator())
                        .build())
                .phoneNumber(in.getPhoneNumber())
                .build();
        return admin;
    }

    private Boolean isEmailDuplicated(String username){
        return userRepository.findByUsername(username).isPresent();
    }


}
