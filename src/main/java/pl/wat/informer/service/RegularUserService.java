package pl.wat.informer.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.wat.informer.dto.form.RegularUserRegisterForm;
import pl.wat.informer.dto.resp.RegisteredUser;
import pl.wat.informer.exception.DuplicatedUserException;
import pl.wat.informer.mapper.RegularUserMapper;
import pl.wat.informer.model.RegularUser;
import pl.wat.informer.model.User;
import pl.wat.informer.repository.RegularUserRepository;
import pl.wat.informer.repository.UserRepository;

@Service
public class RegularUserService {
    private final RegularUserRepository regularUserRepository;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final RoleService roleService;
    private final RegularUserMapper mapper;

    public RegularUserService(RegularUserRepository regularUserRepository, UserRepository userRepository,
                              BCryptPasswordEncoder bCryptPasswordEncoder, RoleService roleService, RegularUserMapper mapper) {
        this.regularUserRepository = regularUserRepository;
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.roleService = roleService;
        this.mapper = mapper;
    }

    public RegisteredUser add(RegularUserRegisterForm regularUserRegisterForm) {
        if (isEmailDuplicated(regularUserRegisterForm.getEmail()))
            throw new DuplicatedUserException(regularUserRegisterForm.getEmail());
        RegularUser user = buildUserToRegister(regularUserRegisterForm);
        return mapper.mapToRegisteredUser(regularUserRepository.save(user));
    }

    private RegularUser buildUserToRegister(RegularUserRegisterForm in) {
        RegularUser user = RegularUser.builder()
                .firstName(in.getFirstName())
                .lastName(in.getLastName())
                .user(User.builder()
                        .username(in.getEmail())
                        .password(bCryptPasswordEncoder.encode(in.getPassword()))
                        .role(roleService.findRoleRegularUser())
                        .build())
                .phoneNumber(in.getPhoneNumber())
                .build();
        return user;
    }

    private Boolean isEmailDuplicated(String username) {
        return userRepository.findByUsername(username).isPresent();
    }
}
