package pl.wat.informer.service;

import org.springframework.stereotype.Service;
import pl.wat.informer.enums.RoleEnum;
import pl.wat.informer.model.Role;
import pl.wat.informer.repository.RoleRepository;


@Service
public class RoleService {
    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role findOrCreateIfNotExist(RoleEnum roleEnum){
        return roleRepository.findByRole(roleEnum.toString()).orElseGet(() -> save(roleEnum));
    }

    private Role save(RoleEnum roleEnum) {
        return roleRepository.save(Role.builder()
                .role(roleEnum.toString())
                .build());
    }

    Role findRoleAdministrator(){
        return findOrCreateIfNotExist(RoleEnum.ROLE_ADMIN);
    }
    Role findRoleRegularUser(){
        return findOrCreateIfNotExist(RoleEnum.ROLE_REGULAR);
    }


}
