package pl.wat.informer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.wat.informer.model.RegularUser;

@Repository
public interface RegularUserRepository extends JpaRepository<RegularUser, Long> {
}
