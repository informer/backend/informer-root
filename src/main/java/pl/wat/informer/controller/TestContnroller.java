package pl.wat.informer.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.wat.informer.dto.resp.Testresponse;

@RestController
@RequestMapping("/test")
public class TestContnroller {

    @GetMapping("/secured")
    public String shouldBeSecured(){
        return "IT IS SECURED";
    }

    @GetMapping("/admin")
    public Testresponse shouldBeSecuredAdmin(){
        return new Testresponse("ADMIN");
    }

    @GetMapping("/admin/sth")
    public Testresponse shouldBeSecuredAdminWithSth(){
        return new Testresponse("ADMIN STH1");
    }

    @GetMapping("/admin/sth1")
    public Testresponse shouldBeSecuredAdminWithSth1(){
        return new Testresponse("ADMIN STH2");
    }


    @GetMapping("/regular")
    public Testresponse shouldBeSecuredRegular(){
        System.out.printf("\n\n\n\n\n\ntest");
        return new Testresponse("REGULAR");
    }

    @GetMapping("/not-secured")
    public Testresponse shouldNotBeSecured(){
        return new Testresponse("NOT SECURED");
    }

}
