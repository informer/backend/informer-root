package pl.wat.informer.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.wat.informer.dto.form.RegularUserRegisterForm;
import pl.wat.informer.dto.resp.RegisteredUser;
import pl.wat.informer.service.RegularUserService;

@RestController
@RequestMapping("/register")
public class RegistrationController {

    private final RegularUserService service;

    public RegistrationController(@RequestBody RegularUserService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<RegisteredUser> registerUser(@RequestBody RegularUserRegisterForm form){
        return ResponseEntity.ok(service.add(form));
    }

}
