package pl.wat.informer.exception;

public class IncorrectOldPassword extends RuntimeException {
    public IncorrectOldPassword(String email) {
        super("Niepoprawne stare hasło dla użytkownika: " + email);

    }
}
