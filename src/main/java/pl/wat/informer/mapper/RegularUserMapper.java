package pl.wat.informer.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.wat.informer.dto.form.RegularUserRegisterForm;
import pl.wat.informer.dto.resp.RegisteredUser;
import pl.wat.informer.model.RegularUser;

@Mapper
public interface RegularUserMapper {
    RegularUser mapToRegularUser (RegisteredUser in );
    @Mapping(source = "user.username", target="email")
    RegisteredUser mapToRegisteredUser(RegularUser in);
}


