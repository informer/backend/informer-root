package pl.wat.informer;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.wat.informer.dto.form.AdminRegisterForm;
import pl.wat.informer.dto.form.RegularUserRegisterForm;
import pl.wat.informer.enums.RoleEnum;
import pl.wat.informer.service.AdministratorService;
import pl.wat.informer.service.RegularUserService;
import pl.wat.informer.service.RoleService;

@Component
public class Starter implements CommandLineRunner {
    private final RoleService roleService;
    private final AdministratorService administratorService;
    private final RegularUserService regularUserService;

    public Starter(RoleService roleService, AdministratorService administratorService, RegularUserService regularUserService) {
        this.roleService = roleService;
        this.administratorService = administratorService;
        this.regularUserService = regularUserService;
    }

    @Override
    public void run(String... args) throws Exception {
        roleService.findOrCreateIfNotExist(RoleEnum.ROLE_ADMIN);
        roleService.findOrCreateIfNotExist(RoleEnum.ROLE_DISPATCHER);
        roleService.findOrCreateIfNotExist(RoleEnum.ROLE_SERVICE);
        roleService.findOrCreateIfNotExist(RoleEnum.ROLE_REGULAR);
        initAdmin();
        initUser();
    }

    private void initAdmin(){
        AdminRegisterForm adminRegisterForm = AdminRegisterForm.builder()
                                                .email("admin@bbb.pl").firstName("User")
                                                .lastName("Userowski").password("password").phoneNumber("123123123").build();
        administratorService.add(adminRegisterForm);
    }

    private void initUser(){
        RegularUserRegisterForm regularUserRegisterForm = RegularUserRegisterForm.builder()
                .email("user@bbb.pl").firstName("Admin")
                .lastName("Administratorski").password("password").phoneNumber("123123123").build();
        regularUserService.add(regularUserRegisterForm);
    }
}
