package pl.wat.informer.enums;

public enum RoleEnum {
    ROLE_ADMIN,
    ROLE_DISPATCHER,
    ROLE_SERVICE,
    ROLE_REGULAR
}
