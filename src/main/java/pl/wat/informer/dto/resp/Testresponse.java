package pl.wat.informer.dto.resp;

import lombok.*;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Testresponse {
    private String response;
}
