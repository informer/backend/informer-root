package pl.wat.informer.dto.resp;

import lombok.*;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RegisteredUser {
    private String email;
    private String firstName;
    private String lastName;
    private String phoneNumber;
}