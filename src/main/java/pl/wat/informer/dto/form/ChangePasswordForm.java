package pl.wat.informer.dto.form;

import lombok.*;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ChangePasswordForm {
    private Integer id;
    private String oldPassword;
    private String newPassword;
}
