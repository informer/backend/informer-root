package pl.wat.informer.dto.form;


import lombok.*;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AdminRegisterForm {
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
}

