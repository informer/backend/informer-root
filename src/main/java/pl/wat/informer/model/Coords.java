package pl.wat.informer.model;

import lombok.*;

import javax.persistence.Embeddable;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Embeddable
public class Coords {
    private String latitude;
    private String longitude;
}
