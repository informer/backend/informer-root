package pl.wat.informer.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table
@Entity(name = "services")
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_service")
    private Long id;

    @Column(name = "notification_description")
    private Integer description;

    @ManyToMany
    private List<IncidentType> incidentTypes;

    @OneToMany(mappedBy="service")
    private List<ServiceUnit> serviceUnits;
}
