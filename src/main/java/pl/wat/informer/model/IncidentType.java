package pl.wat.informer.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table
@Entity(name = "incident_types")
public class IncidentType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_incident_type")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy="incidentType")
    private List<Notification> notifications;

    @ManyToMany
    @JoinTable(
            name = "incident_type_service",
            joinColumns = @JoinColumn(name = "id_incident_type"),
            inverseJoinColumns = @JoinColumn(name = "id_service"))
    private List<Service> services;
}
