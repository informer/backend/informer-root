package pl.wat.informer.model;


import lombok.*;

import javax.persistence.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table
@Entity(name = "notifications")
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_notification")
    private Long id;

    @ManyToOne
    @JoinColumn(name="id_user", nullable=false)
    private RegularUser userReporter;

    @ManyToOne
    @JoinColumn(name="id_incident_type", nullable=false)
    private IncidentType incidentType;

    @Column(name = "notification_description")
    private String description;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_confirmation", referencedColumnName = "id_confirmation")
    private NotificationConfirmation confirmation;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride( name = "latitude", column = @Column(name = "latitude")),
            @AttributeOverride( name = "longitude", column = @Column(name = "longitude")),
            })
    private Coords coords;





}
