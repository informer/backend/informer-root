package pl.wat.informer.model;

import lombok.*;

import javax.persistence.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table
@Entity(name = "notification_confirmations")
public class NotificationConfirmation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_confirmation")
    private Long id;

    @OneToOne(mappedBy = "confirmation")
    private Notification notification;

    @ManyToOne
    @JoinColumn(name="id_service_unit")
    private ServiceUnit serviceUnit;

    @ManyToOne
    @JoinColumn(name="id_user")
    private Dispatcher dispatcher;

}
